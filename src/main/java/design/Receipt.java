/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package design;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class Receipt {
    private int id;
    private String timestamp;
    private Double total;
    private int discount;
    private Double cash;
    private int m_id;
    private int ep_id;

    public Receipt(int id, String timestamp, Double total, int discount, Double cash, int m_id, int ep_id) {
        this.id = id;
        this.timestamp = timestamp;
        this.total = total;
        this.discount = discount;
        this.cash = cash;
        this.m_id = m_id;
        this.ep_id = ep_id;
    }

    public Receipt(String timestamp, Double total, int discount, Double cash, int m_id, int ep_id) {
        this.id = -1;
        this.timestamp = timestamp;
        this.total = total;
        this.discount = discount;
        this.cash = cash;
        this.m_id = m_id;
        this.ep_id = ep_id;
    }
     public Receipt() {
        this.id = -1;
        
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public Double getCash() {
        return cash;
    }

    public void setCash(Double cash) {
        this.cash = cash;
    }

    public int getM_id() {
        return m_id;
    }

    public void setM_id(int m_id) {
        this.m_id = m_id;
    }

    public int getEp_id() {
        return ep_id;
    }

    public void setEp_id(int ep_id) {
        this.ep_id = ep_id;
    }
    public static Receipt fromRS(ResultSet rs) {
        Receipt receipt = new Receipt();
        try {
            receipt.setId(rs.getInt("RC_ID"));
            receipt.setTimestamp(rs.getString("RC_Timestamp"));
            receipt.setTotal(rs.getDouble("RC_Total"));
            receipt.setDiscount(rs.getInt("RC_Discount"));
            receipt.setCash(rs.getDouble("RC_Cash"));
            receipt.setEp_id(rs.getInt("EP_ID"));
            receipt.setM_id(rs.getInt("M_ID"));
            

        } catch (SQLException ex) {
            Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receipt;
    }

    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", timestamp=" + timestamp + ", total=" + total + ", discount=" + discount + ", cash=" + cash + ", m_id=" + m_id + ", ep_id=" + ep_id + '}';
    }
    
    
}
