/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package design;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Admin
 */
public class Checkinout {

    private int id;
    private Date time_start;
    private Date time_end;
    private String hour;
    private String payment_status;
    private int ep_id;
    private int s_id;

    public Checkinout(int id, Date time_start, Date time_end, String hour, String payment_status, int ep_id, int s_id) {
        this.id = id;
        this.time_start = time_start;
        this.time_end = time_end;
        this.hour = hour;
        this.payment_status = payment_status;
        this.ep_id = ep_id;
        this.s_id = s_id;
    }

    public Checkinout(Date time_start, Date time_end, String hour, String payment_status, int ep_id, int s_id) {
        this.id = -1;
        this.time_start = time_start;
        this.time_end = time_end;
        this.hour = hour;
        this.payment_status = payment_status;
        this.ep_id = ep_id;
        this.s_id = s_id;
    }

    public Checkinout() {
        this.id = -1;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTime_start() {
        return time_start;
    }

    public void setTime_start(Date time_start) {
        this.time_start = time_start;
    }

    public Date getTime_end() {
        return time_end;
    }

    public void setTime_end(Date time_end) {
        this.time_end = time_end;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public int getEp_id() {
        return ep_id;
    }

    public void setEp_id(int ep_id) {
        this.ep_id = ep_id;
    }

    public int getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }

    public static Checkinout fromRS(ResultSet rs) {
        Checkinout checkin_out = new Checkinout();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        try {
            checkin_out.setId(rs.getInt("CIO_ID"));
            String in = rs.getString("CIO_Time_Start");
            String out = rs.getString("CIO_Time_End");
            try {
                checkin_out.setTime_start(df.parse(in));
                checkin_out.setTime_end(df.parse(out));
            } catch (ParseException e) {

            }
            checkin_out.setHour(rs.getString("CIO_Hour"));
            checkin_out.setPayment_status(rs.getString("CIO_Payment_Status"));
            checkin_out.setEp_id(rs.getInt("EP_ID"));
            checkin_out.setS_id(rs.getInt("S_ID"));

        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkin_out;
    }

    @Override
    public String toString() {
        return "CheckIn_Out{" + "id=" + id + ", time_start=" + time_start + ", time_end=" + time_end + ", hour=" + hour + ", payment_status=" + payment_status + ", ep_id=" + ep_id + ", s_id=" + s_id + '}';
    }

    public Object getFirstname(int ep_id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}