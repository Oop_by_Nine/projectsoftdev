/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package design;
import dao.EmpolyeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Admin
 */
public class Checklist {

    private int id;
    private Date date;
    private Employee employee;

    public Checklist(int id, Date date, Employee employee) {
        this.id = id;
        this.date = date;
        this.employee = employee;
//        this.checkListItem = checkListItem;
    }

//    public void addCheckListItem(CheckListItem checkListItem) {
//        this.checkListItem.add(checkListItem);
//        
//        total = total + checkListItem.getTotal();
//        qty = qty + orderDetail.getQty();
//    }
//
//    public void addOrderDetail(Product product, String productName, double productPrice, int qty) {
//        OrderDetail orderDetail = new OrderDetail(product, productName, productPrice, qty, this);
//        this.addOrderDetail(orderDetail);
//    }
//
//    public void addOrderDetail(Product product, int qty) {
//        OrderDetail orderDetail = new OrderDetail(product, product.getName(), product.getPrice(), qty, this);
//        this.addOrderDetail(orderDetail);
//    }
    public Checklist() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "CheckList{" + "id=" + id + ", date=" + date + '}';
    }

    public static Checklist fromRS(ResultSet rs) {
        Checklist checkList = new Checklist();
        Employee emp = new Employee();
        EmpolyeeDao empD = new EmpolyeeDao();
        try {
            int empID = rs.getInt("EP_ID");
            emp = empD.get(empID);
            checkList.setId(rs.getInt("CL_ID"));
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//            String xxx = rs.getString("CL_Date");
//
//            Date date = sdf.parse(xxx);
            String date = rs.getString("CL_Date");
            checkList.setDate(sdf.parse(date));
            checkList.setEmployee(emp);
        } catch (SQLException ex) {
            Logger.getLogger(Checklist.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (ParseException ex) { 
            Logger.getLogger(Checklist.class.getName()).log(Level.SEVERE, null, ex);
        }
        return checkList;
    }
}

