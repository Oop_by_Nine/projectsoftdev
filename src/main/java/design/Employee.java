/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package design;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Admin
 */
public class Employee {
    private int id;
    private String firstname;
    private String lastname;
    private String gender;
    private String identitynum;
    private String email;
    private String dateofbirth;
    private double hourrate;

    public Employee(String firstname, String lastname, String gender, String identitynum, String email, String dateofbirth) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.gender = gender;
        this.identitynum = identitynum;
        this.email = email;
        this.dateofbirth = dateofbirth;
        this.hourrate = hourrate;
    }

    public Employee(String firstname, String lastname, String gender, String identitynum, String email, String dateofbirth, double hourrate) {
        this.id = -1;
        this.firstname = firstname;
        this.lastname = lastname;
        this.gender = gender;
        this.identitynum = identitynum;
        this.email = email;
        this.dateofbirth = dateofbirth;
        this.hourrate = hourrate;
    }
    public Employee() {
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getGender() {
        return gender;
    }

    public String getIdentitynum() {
        return identitynum;
    }

    public String getEmail() {
        return email;
    }

    public String getDateofbirth() {
        return dateofbirth;
    }

    public double getHourrate() {
        return hourrate;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setIdentitynum(String identitynum) {
        this.identitynum = identitynum;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDateofbirth(String dateofbirth) {
        this.dateofbirth = dateofbirth;
    }

    public void setHourrate(double hourrate) {
        this.hourrate = hourrate;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", gender=" + gender + ", identitynum=" + identitynum + ", email=" + email + ", dateofbirth=" + dateofbirth + ", hourrate=" + hourrate + '}';
    }

    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("EP_ID"));
            employee.setFirstname(rs.getString("EP_Firstname"));
            employee.setLastname(rs.getString("Ep_Lastname"));
            employee.setGender(rs.getString("EP_Gender"));
            employee.setIdentitynum(rs.getString("EP_IdentityNum"));
            employee.setEmail(rs.getString("EP_Email"));
            employee.setDateofbirth(rs.getString("EP_Dateofbirth"));
            employee.setHourrate(rs.getDouble("EP_Hour_Rate"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }



}
