/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;
import database.Dataconnection;
import design.Checkinout;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Admin
 */
public class CheckinoutDao implements Dao<Checkinout> {

    @Override
    public Checkinout get(int id) {
        Checkinout item = null;
        String sql = "SELECT * FROM CheckIn_Out WHERE CIO_ID=?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Checkinout.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<Checkinout> getAll() {
        ArrayList<Checkinout> list = new ArrayList();
        String sql = "SELECT * FROM CheckIn_Out";
        Connection conn = Dataconnection.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Checkinout item = Checkinout.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

    @Override
    public Checkinout save(Checkinout obj) {
        String sql = "INSERT INTO CheckIn_Out (CIO_Time_Start, CIO_Time_End, CIO_Hour,CIO_Payment_Status,EP_ID)" 
                + "VALUES ( ?, ?, ?, ?, ?)";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setString(1, df.format(obj.getTime_start()));
            stmt.setString(2, df.format(obj.getTime_end()));
            stmt.setString(3, obj.getHour());
            stmt.setString(4, obj.getPayment_status());
            stmt.setInt(5, obj.getEp_id());

            stmt.executeUpdate();
            int id = Dataconnection.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Checkinout update(Checkinout obj) {
        String sql = "UPDATE CheckIn_Out"
                + " SET CIO_Time_Start = ? ,CIO_Time_End = ? ,CIO_Hour = ?,CIO_Payment_Status = ?,EP_ID = ?,S_ID = ?"
                + " WHERE CIO_ID = ?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            stmt.setString(1, df.format(obj.getTime_start()));
            stmt.setString(2, df.format(obj.getTime_end()));
            stmt.setString(3, obj.getHour());
            stmt.setString(4, obj.getPayment_status());
            stmt.setInt(5, obj.getEp_id());
            stmt.setInt(6, obj.getS_id());
            stmt.setInt(7, obj.getId());

            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Checkinout obj) {
        String sql = "DELETE FROM CheckIn_Out WHERE CIO_ID=?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Checkinout> getAll(String where, String order) {
        ArrayList<Checkinout> list = new ArrayList();
        String sql = "SELECT * FROM CheckIn_Out where " + where + " ORDER BY" + order;
        Connection conn = Dataconnection.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Checkinout item = Checkinout.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
