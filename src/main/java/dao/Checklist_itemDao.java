/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import database.Dataconnection;
import design.Checklist_item;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Admin
 */
public class Checklist_itemDao implements Dao<Checklist_item>{
    @Override
    public Checklist_item get(int id) {
        Checklist_item item = null;
        String sql = "SELECT * FROM Check_List_Item WHERE CLI_ID=?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Checklist_item.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public List<Checklist_item> getAll() {
        ArrayList<Checklist_item> list = new ArrayList();
        String sql = "SELECT * FROM Check_List_Item";
        Connection conn = Dataconnection.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Checklist_item item = Checklist_item.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Checklist_item> getAll(String where, String order) {
        ArrayList<Checklist_item> list = new ArrayList();
        String sql = "SELECT * FROM Check_List_Item where " + where + " ORDER BY " + order;
        Connection conn = Dataconnection.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Checklist_item item = Checklist_item.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    


    @Override
    public Checklist_item save(Checklist_item checkListItem) {
        String sql = "INSERT INTO Check_List_Item (CLI_ID,CLI_Quantity,MT_Unit,MT_Name,MT_Min,MT_QOH,CL_ID,MT_ID)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, checkListItem.getId());
            stmt.setInt(2, checkListItem.getQuantity());
            stmt.setString(3, checkListItem.getUnit());
            stmt.setString(4, checkListItem.getName());
            stmt.setInt(5, checkListItem.getMin());
            stmt.setInt(6, checkListItem.getQoh());
            stmt.setInt(7, checkListItem.getChecklist().getId());
            stmt.setInt(8, checkListItem.getMaterial().getId());
            stmt.executeUpdate();
            int id = Dataconnection.getInsertedId(stmt);
            checkListItem.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return checkListItem;
    }

    @Override
    public Checklist_item update(Checklist_item checkListItem) {
        String sql = "UPDATE Check_List_Item"
                + " SET CLI_Quantity = ?, MT_Unit = ?, MT_Name = ?, MT_Min = ?, MT_QOH = ? , CL_ID = ?, MT_ID = ?"
                + " WHERE CLI_ID = ?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            
            stmt.setInt(1, checkListItem.getQuantity());
            stmt.setString(2, checkListItem.getUnit());
            stmt.setString(3, checkListItem.getName());
            stmt.setInt(4, checkListItem.getMin());
            stmt.setInt(5, checkListItem.getQoh());
            stmt.setInt(6, checkListItem.getChecklist().getId());
            stmt.setInt(7, checkListItem.getMaterial().getId());
            stmt.setInt(8, checkListItem.getId());
            
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = Dataconnection.getInsertedId(stmt);
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Checklist_item checkListItem) {
        String sql = "DELETE FROM Check_List_Item WHERE CLI_ID=?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, checkListItem.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;      
    }
}
