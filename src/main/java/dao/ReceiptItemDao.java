/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import database.Dataconnection;
import design.ReceiptItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Admin
 */
public class ReceiptItemDao implements Dao<ReceiptItem> {

    @Override
    public ReceiptItem get(int id) {
        ReceiptItem item = null;
        String sql = "SELECT * FROM receipt_item WHERE RCIT_ID=?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = ReceiptItem.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<ReceiptItem> getAll() {
        ArrayList<ReceiptItem> list = new ArrayList();
        String sql = "SELECT * FROM receipt_item";
        Connection conn = Dataconnection.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptItem item = ReceiptItem.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public ReceiptItem save(ReceiptItem obj) {
        String sql = "INSERT INTO receipt_item (PD_Name,RCIT_Amount,RCIT_Price,RCIT_Total,RC_ID,PD_ID)"
                + "VALUES (?, ?, ?, ?, ?, ?)";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, obj.getPd_name());
            stmt.setInt(2, obj.getAmount());
            stmt.setDouble(3, obj.getPrice());
            stmt.setDouble(4, obj.getTotal());
            stmt.setInt(5, obj.getRc_id());
            stmt.setInt(6, obj.getPd_id());

            stmt.executeUpdate();
            int id = Dataconnection.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public ReceiptItem update(ReceiptItem obj) {
        String sql = "UPDATE receipt_item"
                + " SET PD_Name = ? ,RCIT_Amount = ? ,RCIT_Price = ? ,RCIT_Total = ?,RC_ID = ?,PD_ID = ?"
                + " WHERE RCIT_ID = ?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, obj.getPd_name());
            stmt.setInt(2, obj.getAmount());
            stmt.setDouble(3, obj.getPrice());
            stmt.setDouble(4, obj.getTotal());
            stmt.setInt(5, obj.getRc_id());
            stmt.setInt(6, obj.getPd_id());
            stmt.setInt(7, obj.getId());

            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(ReceiptItem obj) {
        String sql = "DELETE FROM receipt_item WHERE RCIT_ID =?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<ReceiptItem> getAll(String where, String order) {
        ArrayList<ReceiptItem> list = new ArrayList();
        String sql = "SELECT * FROM receipt_item where " + where + " ORDER BY" + order;
        Connection conn = Dataconnection.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptItem item = ReceiptItem.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
