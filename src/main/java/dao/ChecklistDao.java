/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import database.Dataconnection;
import design.Checklist;
import design.Checklist_item;
import design.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Admin
 */
public class ChecklistDao implements Dao<Checklist>  {
    @Override
    public Checklist get(int id) {
        Checklist item = null;
        String sql = "SELECT * FROM Check_List WHERE CL_ID=?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Checklist.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    public List<Checklist> getAll() {
        ArrayList<Checklist> list = new ArrayList();
        String sql = "SELECT * FROM Check_List";
        Connection conn = Dataconnection.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Checklist item = Checklist.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<Checklist> getAll(String where, String order) {
        ArrayList<Checklist> list = new ArrayList();
        String sql = "SELECT * FROM Check_List where " + where + " ORDER BY " + order;
        Connection conn = Dataconnection.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Checklist item = Checklist.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    


    @Override
    public Checklist save(Checklist checkList) {
        String sql = "INSERT INTO Check_List (CL_ID,CL_Date,EP_ID)"
                + "VALUES(?, ?, ?)";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, checkList.getId());
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            stmt.setString(2, sdf.format(checkList.getDate()));
            stmt.setInt(3, checkList.getEmployee().getId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = Dataconnection.getInsertedId(stmt);
            checkList.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return checkList;
    }

    @Override
    public Checklist update(Checklist checkList) {
        String sql = "UPDATE Check_List"
                + " SET CL_Date = ?, EP_ID = ?"
                + " WHERE CL_ID = ?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(3, checkList.getId());
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            stmt.setString(1, sdf.format(checkList.getDate()));
            stmt.setInt(2, checkList.getEmployee().getId());

//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = Dataconnection.getInsertedId(stmt);
//            checkList.setId(id);
            //Add Item
//            checkList.setId(id);
//            for(CheckListItem od:checkList.getOrderDetails()){
//                CheckListItem detail = orderDetailDao.save(od);
//                if(detail == null){
//                    DatabaseHelper.endTransactionWithRollback();
//                    return null;
//                }
//            }
//            DatabaseHelper.endTransactionWithCommit();
            
            return get(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Checklist checkList) {
        String sql = "DELETE FROM Check_List WHERE CL_ID=?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, checkList.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

}
