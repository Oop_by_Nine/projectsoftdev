/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import database.Dataconnection;
import design.Receipt;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Admin
 */
public class ReceiptDao  implements Dao<Receipt>{

    @Override
    public Receipt get(int id) {
        Receipt receipt = null;
        String sql = "SELECT * FROM Receipt WHERE RC_ID=?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                receipt = Receipt.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return receipt;
    }

    @Override
    public List<Receipt> getAll() {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM Receipt";
        Connection conn = Dataconnection.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;    
    }

    @Override
    public Receipt save(Receipt obj) {
        String sql = "INSERT INTO receipt (RC_Timestamp,RC_Total,RC_Discount,RC_Cash,EP_ID, M_ID)"
                + "VALUES (?, ?, ?, ?, ?,?)";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            
            stmt.setString(1, obj.getTimestamp());
            stmt.setDouble(2, obj.getTotal());
            stmt.setInt(3, obj.getDiscount());
            stmt.setDouble(4, obj.getCash());
            stmt.setInt(5, obj.getEp_id());
            stmt.setInt(6, obj.getM_id());
            
            
            stmt.executeUpdate();
            int id = Dataconnection.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;    }

    @Override
    public Receipt update(Receipt obj) {
String sql = "UPDATE Receipt"
                + " SET RC_Timestamp = ?, RC_Total = ? , RC_Discount = ? ,RC_Cash = ?, EP_ID = ?, M_ID = ?"
                + " WHERE RC_ID = ?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getTimestamp());
            stmt.setDouble(2, obj.getTotal());
            stmt.setInt(3, obj.getDiscount());
            stmt.setDouble(4, obj.getCash());
            stmt.setInt(5, obj.getEp_id());
            stmt.setInt(6, obj.getM_id());
            stmt.setInt(7, obj.getId());
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }    }

    @Override
    public int delete(Receipt obj) {
        String sql = "DELETE FROM receipt WHERE RC_ID=?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;    }

    @Override
    public List<Receipt> getAll(String where, String order) {
    ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM receipt where " + where + " ORDER BY" + order;
        Connection conn = Dataconnection.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt receipt = Receipt.fromRS(rs);
                list.add(receipt);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;    }
    
}
