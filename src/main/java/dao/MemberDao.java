/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import database.Dataconnection;
import design.Member;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Admin
 */
public class MemberDao implements Dao<Member> {

    @Override
    public Member get(int id) {
        Member item = null;
        String sql = "SELECT * FROM Member WHERE M_ID = ?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = Member.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return item;
    }

    @Override
    public List<Member> getAll() {
        ArrayList<Member> list = new ArrayList();
        String sql = "SELECT * FROM member";
        Connection conn = Dataconnection.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Member item = Member.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Member save(Member obj) {
   String sql = "INSERT INTO member (M_FirstName, M_LastName, M_Phone, M_Score)"
                + "VALUES(?, ?, ?, ?)";
       Connection conn = Dataconnection.getConnect();
       try {
           PreparedStatement stmt = conn.prepareStatement(sql);
           stmt.setString(1, obj.getFirstname());
           stmt.setString(2, obj.getLastname());
            stmt.setString(3, obj.getPhone());
            stmt.setInt(4, obj.getScore());
        
           stmt.executeUpdate();
            int id = Dataconnection.getInsertedId(stmt);
           obj.setId(id);
       } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
       }
        return obj;
    
    }

    @Override
    public Member update(Member obj) {
      String sql = "UPDATE member"
                + " SET M_FirstName = ?, M_LastName = ?, M_Phone = ? , M_Score = ?"
                + " WHERE M_ID = ?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getFirstname());
            stmt.setString(2, obj.getLastname());
            stmt.setString(3, obj.getPhone());
            stmt.setInt(4, obj.getScore());
            stmt.setInt(5, obj.getId());
               
            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }

    }

    @Override
    public int delete(Member obj) {
        String sql = "DELETE FROM member WHERE M_ID=?";
        Connection conn = Dataconnection.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }


    @Override
    public List<Member> getAll(String where, String order) {
        ArrayList<Member> list = new ArrayList();
        String sql = "SELECT * FROM member where " + where + " ORDER BY" + order;
        Connection conn = Dataconnection.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Member item = Member.fromRS(rs);
                list.add(item);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }   

}
